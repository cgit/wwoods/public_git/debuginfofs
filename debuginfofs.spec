Name:           debuginfofs
Version:        0.2
Release:        1%{?dist}
Summary:        Network-mountable filesystem for debuginfo

Group:          Development/Debuggers
License:        GPLv2+
URL:            http://fedorapeople.org/gitweb?p=wwoods/public_git/debuginfofs.git
Source0:        %{name}-%{version}.tar.bz2
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

Requires:         davfs2
Requires(post):   chkconfig
Requires(preun):  chkconfig
Requires(preun):  initscripts
Requires(postun): initscripts

%description
debuginfofs is a network-mountable filesystem that provides the debugging
symbols needed to generate a fully annotated backtrace.

%package server
Summary:        Server setup for debuginfofs
Group:          Development/Debuggers
Requires:         rpm cpio httpd
Requires(post):   chkconfig
Requires(post):   policycoreutils
Requires(preun):  chkconfig
Requires(preun):  policycoreutils
Requires(preun):  initscripts
Requires(postun): initscripts

%description server
Server tools and config files for running a debuginfofs server

%prep
%setup -q


%build
# not needed

%install
rm -rf $RPM_BUILD_ROOT
# client
install -D -m 0755 client/debuginfofs.init $RPM_BUILD_ROOT%{_initrddir}/debuginfofs
install -D -m 0644 client/debuginfofs.sysconfig $RPM_BUILD_ROOT/etc/sysconfig/debuginfofs
install -D -m 0644 client/davfs2-debuginfofs.conf $RPM_BUILD_ROOT/etc/davfs2/debuginfofs.conf
# server
mkdir -p $RPM_BUILD_ROOT{/usr/share/debuginfofs,/usr/sbin}
mkdir -p $RPM_BUILD_ROOT/var/{www,cache}/debuginfofs
#uncomment when this is ready
#install -m 0755 server/repofs.py $RPM_BUILD_ROOT/usr/sbin
install -m 0755 server/debuginfofs-mirror $RPM_BUILD_ROOT/usr/sbin
install -m 0755 server/debuginfofs-cleanup $RPM_BUILD_ROOT/usr/sbin
install -m 0644 server/debuginfofs.conf $RPM_BUILD_ROOT/etc
install -m 0644 server/dav-debuginfo.conf $RPM_BUILD_ROOT/usr/share/debuginfofs
install -m 0755 server/debuginfofs-server.init $RPM_BUILD_ROOT%{_initrddir}/debuginfofs-server

 
%clean
rm -rf $RPM_BUILD_ROOT

%post
/sbin/chkconfig --add debuginfofs
%preun
if [ "$1" == "0" ] ; then
    /sbin/service debuginfofs stop >/dev/null 2>&1
    /sbin/chkconfig --del debuginfofs
fi
%postun
if [ "$1" -ge "1" ] ; then
    /sbin/service debuginfofs condrestart >/dev/null 2>&1 || :
fi

%post server
/sbin/chkconfig --add debuginfofs-server
/usr/sbin/semanage port -a -S targeted -t http_port_t -p tcp 3309 &> /dev/null || :
%preun server
if [ "$1" == "0" ] ; then
    /sbin/service debuginfofs-server stop >/dev/null 2>&1
    /sbin/chkconfig --del debuginfofs-server
fi
/usr/sbin/semanage port -d -t http_port_t -p tcp 3309 &> /dev/null || :
%postun server
if [ "$1" -ge "1" ] ; then
    /sbin/service debuginfofs-server condrestart >/dev/null 2>&1 || :
fi


%files
%defattr(-,root,root,-)
%doc README.client
%{_initrddir}/debuginfofs
%config(noreplace) /etc/davfs2/debuginfofs.conf
%config(noreplace) /etc/sysconfig/debuginfofs

%files server
%defattr(-,root,root,-)
%doc README TODO COPYING
%dir /var/www/debuginfofs
%dir /var/cache/debuginfofs
%{_initrddir}/debuginfofs-server
%config(noreplace) /etc/debuginfofs.conf
%config(noreplace) /usr/share/debuginfofs/dav-debuginfo.conf
%{_sbindir}/debuginfofs-mirror
%{_sbindir}/debuginfofs-cleanup

%changelog
* Fri Mar 06 2009 Will Woods <wwoods@redhat.com> - 0.2-1
- Client: switch to davfs2 (uses disk cache to reduce bandwidth/memory use)
- Server: add debuginfofs-server service, uses port 3309
- Server: add debuginfofs-cleanup script to remove expired debuginfo

* Tue Mar 03 2009 Will Woods <wwoods@redhat.com> - 0.1-1
- Package Review: Add COPYING, fix up initscript, clean up specfile

* Mon Feb 23 2009 Will Woods <wwoods@redhat.com> - 0.1-0
- Initial package creation
